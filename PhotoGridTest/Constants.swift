//
//  Constants.swift
//  PhotoGridTest
//
//  Created by Konstantin Myskin on 21/11/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    struct pixabay {
        static let host = "https://pixabay.com/api/"
        static let key = "10760886-8b8108bb04d815626bb84240d" 
        static let photosPerPage = 10 // decrease to make the size of the photo chunks smaller 
    }
    
    static var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    static var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
}

struct Noti { //NSNotification convenience 
    static let refreshCell = "refreshCell"
    static let reloadCollectionWithCount = "reloadCollectionWithCount" 
    
}

enum PhotoServiceTag {
    case pixabay
    // ...
}

