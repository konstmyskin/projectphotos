//
//  PhotoServices.swift
//  PhotoGridTest
//
//  Created by Konstantin Myskin on 21/11/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import Foundation
import UIKit

class PhotoService { // could use protocol here
    let tag: PhotoServiceTag
    let host: String
    let apiKey: String
    let photosPerPage: Int // decrease to make the size of the photo chunks smaller
    
    var requestedPages = Set<Int>()
    var ovarallPhotoCount = 0
    
    init(tag tagToKnow:PhotoServiceTag, hostAddress:String, apiKeyValue:String, perPage:Int) {
        tag = tagToKnow; host = hostAddress; apiKey = apiKeyValue; photosPerPage = perPage
    }
    func page(for index:Int) -> Int {
        preconditionFailure("This method must be overridden")
    }
    func call(for index:Int, page:Int) -> String? {
        preconditionFailure("This method must be overridden")
    }
}

final class Pixabay : PhotoService {
    static let shared = Pixabay.create()
    
    static func create() -> Pixabay {
        return Pixabay.init(tag:.pixabay, hostAddress: Constants.pixabay.host, apiKeyValue: Constants.pixabay.key, perPage: Constants.pixabay.photosPerPage) 
    }
    override func page(for index:Int) -> Int {
        return index / photosPerPage + 1 // api requires to start from 1
    }
    override func call(for index:Int, page:Int) -> String? {
        let dm = DataManager.shared
        
        guard let number = dm.whichChunk(for: index) else {
            return nil
        }
        
        var realPage = page
        if realPage > 1, realPage * dm.currentPhotoSource.photosPerPage > dm.chunks[0]!.count {
            var previousChunksCount = 0            //fast hack to recall the actual page number here
            for i in 0 ..< dm.chunks.count + (number - dm.chunks.count) {
                previousChunksCount += dm.chunks[i]!.count
            }
            let currentChunkIndex = index - previousChunksCount
            realPage = self.page(for:currentChunkIndex)
        }
        
        
        return self.host + "?key=" + self.apiKey + "&q=" + dm.currentSearchWords[number] + "&image_type=photo" + "&per_page=\(photosPerPage)" + "&page=\(realPage)"
    }
}

// add here as many photo service sources as needed 
 



struct Photo { 
    var link:String 
    var frame:CGRect?
    var isLoading:Bool 
    // ... 
}

struct Chunk {          // all photos for one request (f.e. for word "retro") 
    var name:String
    var count:Int
}

