//
//  PhotoCollectionVC.swift
//  PhotoGridTest
//
//  Created by Konstantin Myskin on 21/11/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import UIKit

class PhotoCollectionVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let currentPhotoSource = DataManager.shared.currentPhotoSource //shortcut
    
    @IBOutlet weak var collectionView: UICollectionView! 
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadIndex(noti:)), name: Notification.Name(rawValue: Noti.refreshCell), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCollection), name: Notification.Name(rawValue: Noti.reloadCollectionWithCount), object: nil)
        let side = Constants.screenWidth / 4 - 2
        layout.itemSize = CGSize(width: side, height: side)
    }
    
    @objc func reloadCollection() {
        self.previousScrollMoment = Date()
        self.previousScrollX = 0
        self.velocity = 0
        self.collectionView.reloadData()
    }
    
    @objc func reloadIndex(noti:Notification) {
        guard let index = noti.object as? Int else {
            print("Index is not valid")
            return
        }
        // reload directly page here
        //            self.collectionView.reloadData()
        self.collectionView.reloadItems(at: [IndexPath.init(item: index, section: 0)])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = DataManager.shared.countChunks()
        return count > 0 ? count : 1 
    }
    
    func photoCell(with index:Int) -> PhotoCollectionCell {
        return self.collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PhotoCollectionCell.self), for: IndexPath(item: index, section: 0)) as! PhotoCollectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = photoCell(with: indexPath.item)
        cell.photoView.image = nil
        if let image = NetworkManager.shared.loadPhotoObject(index: indexPath.item, source: currentPhotoSource, onlyFromCache:velocity > 300) {
            cell.photoView.alpha = 0.0
            UIView.animate(withDuration: 0.1) {
                cell.photoView.image = image
                cell.photoView.alpha = 1.0 
            }
            NetworkManager.shared.serialQueue.async {
                self.preloadPhotosAround(thisOne: indexPath.item) 
            }
        }
        return cell
    }
    
    func preloadPhotosAround(thisOne index:Int) {
        let indexBefore = index - currentPhotoSource.photosPerPage
//        let indexAfter = index + currentPhotoSource.photosPerPage
        if indexBefore > 0 {
            _ = NetworkManager.shared.loadPhotoObject(index: indexBefore, source: currentPhotoSource, onlyFromCache:velocity == 0)
        }
//        if indexAfter < DataManager.shared.countChunks() {
//            _ = NetworkManager.shared.loadPhotoObject(index: indexAfter, source: currentPhotoSource, onlyFromCache:velocity == 0)
//        }
    }
    
    var previousScrollMoment: Date = Date()
    var previousScrollX: CGFloat = 0
    var velocity: CGFloat = 0
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let d = Date()
        let x = scrollView.contentOffset.y
        let elapsed = Date().timeIntervalSince(previousScrollMoment)
        let distance = (x - previousScrollX)
        if previousScrollX == 0 {
            previousScrollX = 1
            return
        }
        velocity = (elapsed == 0) ? 0 : abs(distance / CGFloat(elapsed))  // check the scrolling speed to skip unnecessary loading
        previousScrollMoment = d
        previousScrollX = x
//        print("vel \(velocity)")
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        let side = Constants.screenHeight / 4 - 2
        layout.itemSize = CGSize(width: side, height: side)
    }
 

}

 
