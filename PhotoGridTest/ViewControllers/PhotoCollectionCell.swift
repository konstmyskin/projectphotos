//
//  PhotoCollectionCell.swift
//  PhotoGridTest
//
//  Created by Konstantin Myskin on 21/11/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {  
    
    @IBOutlet weak var photoView: UIImageView!
    
}
