//
//  NetworkManager.swift
//  PhotoGridTest
//
//  Created by Konstantin Myskin on 21/11/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved. 
//

import UIKit
import Foundation

final class NetworkManager {
    static let shared = NetworkManager()
    
    let dm = DataManager.shared
    let serialQueue = DispatchQueue(label: "com.konstmyskin.test")
    
    private init() {
        //check if offline here
    }
    
    func loadPhotoObject(index:Int, source:PhotoService, onlyFromCache:Bool) -> UIImage? {
        if let ph = dm.photoDB[index] { // check if in db
            if ph.isLoading {
                return nil
            } else if let imageFromCache = dm.imageCache.object(forKey: ph.link as NSString) as? UIImage { // check cache
                return imageFromCache
            } else { //seems that photo was flushed from cache, loading it again
                self.downloadImage(photo: ph, index: index)
                return nil
            }
        }
        
        let page = source.page(for: index) 
        guard !onlyFromCache, !source.requestedPages.contains(page), let string = source.call(for: index, page:page), let url = URL(string:string) else {
            //            print("Check the Pixabay url")
            return nil
        }
        
        source.requestedPages.insert(page)                                                                       // start if OK
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                source.requestedPages.remove(page)
                print("Error with photo loading" + (error?.localizedDescription ?? "")) // + try to reload here
                return
            }
            do {
                if data == nil {
                    source.requestedPages.remove(page)
                    print("Critical loading error here")
                    return
                }
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                
                let dict = json as! [String: AnyObject]
                guard let total = dict["totalHits"] else { //count for the whole chunk 
                    print("Error with photo parsing")
                    return
                }
                source.ovarallPhotoCount = total as! Int
                guard let hits = dict["hits"] else {
                    print("Error with photo parsing")
                    return
                }
                let photos = hits as! Array<Dictionary<String, Any>> //parsing
                for i in 0 ..< photos.count {
                    let ind = i + source.photosPerPage * (page - 1)
                    let photo = photos[i]
                    let link = photo["previewURL"] as! String
                    let object = Photo(link: link, frame: nil, isLoading:true) //frame can be used lately to add fullscreen
                    self.dm.photoDB[ind] = object
                    self.downloadImage(photo: object, index: ind) // start data loading
                }
                let ovarallPhotoCount = total as! Int
                if let number = self.dm.whichChunk(for: index) {
                    if self.dm.chunks[number] == nil {
                        let chunk = Chunk.init(name: self.dm.currentSearchWords[number], count: ovarallPhotoCount)
                        self.dm.chunks[number] = chunk
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name:Notification.Name(rawValue: Noti.reloadCollectionWithCount), object:nil) // update        collection with the new count
                        }
                    }
                    
                    if self.dm.currentChunk <= number, Double(index) > Double(self.dm.countChunks()) * 0.9 {
                        //tricky piece - check if more than 90% of existing chunks are behind and load first part of the next one
                        _ = self.loadPhotoObject(index: self.dm.countChunks() + 1, source: DataManager.shared.currentPhotoSource, onlyFromCache: false)
                        self.dm.currentChunk += 1
                    }
                } 
                
                
            } catch let jsonError {
                print(jsonError)
            }
        }).resume()
        
        return nil
    }
    
    func downloadImage(photo:Photo, index:Int) {
        guard let url = URL(string:photo.link) else {
            print("Link error, handle this")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            if var ph = self.dm.photoDB[index] {
                ph.isLoading = false
                if let response = data {
                    if let imageToCache = UIImage(data: response) {
                        self.dm.imageCache.setObject(imageToCache, forKey: ph.link as NSString)
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name:Notification.Name(rawValue: Noti.refreshCell), object:index)
                        }
                    } else {
                        print("Photo load error, handle this")
                    }
                }
                self.dm.photoDB[index] = ph 
            }
            }.resume()
    }
}

