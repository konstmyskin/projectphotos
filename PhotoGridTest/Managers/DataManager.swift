//
//  DataManager.swift
//  PhotoGridTest
//
//  Created by Konstantin Myskin on 21/11/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import Foundation

final class DataManager {
    static let shared = DataManager()
    var photoDB = Dictionary<Int, Photo>() // should be potentially leveled deeper for other photoservices support
    
    var chunks = Dictionary<Int, Chunk>()
    var currentChunk = 0
    
    let imageCache = NSCache<NSString, AnyObject>() // these three params can be unique for each PhotoService instance (to purge the cache separately for example)
    
    private init() {
        imageCache.countLimit = 500  
    }
    
    var currentSearchWords = ["retro", "photo", "culture", "happiness", "life", "love", "nature", "sky", "joy", "fun", "california", "surfing"] 
    var currentPhotoSource = Pixabay.shared // can be choosen from UI potentially
    
    func countChunks() -> Int {
        var result = 0
        for chunk in chunks.values {
            result += chunk.count
        }
        return result
    }
    
    func whichChunk(for index:Int) -> Int? {
        guard index > 0 else {
            return 0
        }
        var overall = 0
        for i in (0 ..< chunks.count) {
            overall += chunks[i]!.count
            if i < currentSearchWords.count {
                if index < overall {
                    return i
                } 
            }
        }
        return chunks.count
    }
}


